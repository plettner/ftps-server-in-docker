# The use of this Makefile is optional.  It serves as an easy
# way to invoke a variety of Docker commands to manage this
# particular container.  And failing that, it also functions
# as a cheat-sheet for remembering various commands.


# This is the name of the container we'll instantiate
CONTAINER=ftps

# The command to invoke Docker (on my system, we need to invoke as root)
# Exception:  docker-compose is used to start the container.
CMD=sudo docker


#
# Using Docker Compose to configure and run our container.
# The -d tells Docker Compose to start the container in
# "detached" mode.
#
.PHONY: start
start:
	sudo docker-compose up -d
 
.PHONY: stop
stop:
	$(CMD) stop $(CONTAINER)
	$(CMD) rm $(CONTAINER)

.PHONY: rm
rm:
	$(CMD) rm $(CONTAINER)
 
.PHONY: logs
logs:
	$(CMD) container logs $(CONTAINER)
 
.PHONY: login
login:
	$(CMD) exec -it $(CONTAINER) bash
 
.PHONY: status
status:
	$(CMD) ps -f name=$(CONTAINER)

