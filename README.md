# FTPS Server as a Docker Container

This project provides everything needed to run an FTPS server
inside a Docker container.  This project was created for and
was tested on Linux.  Some things (particularly the use of 
`sudo`) will need to change for other operating systems.

This project is relatively simple.  The `docker-compose.yml`
file describes the container.  A script, `transfer.sh`, is 
provided to make it easy to run the `lftp` command line file
transfer program to test the container.  (Filezilla can be
used as well.)


## Installing Docker and Docker Compose

[Docker Installation Instructions](https://docs.docker.com/get-docker/)

[Docker Compose Installation Instructions](https://dockerlabs.collabnix.com/intermediate/workshop/DockerCompose/How_to_Install_Docker_Compose.html)


## Setup

You will need to edit the following files in order to set a
password for the FTPS server.  It must be the same password
in all of the following files.  Look for the string 
<SET YOUR PASSWORD HERE> in each file and change it.

    docker-compose.yml
    transfer.sh


## How to Use this Container

To download and run the FTPS container, use the docker-compose command.  The `-d` parameter tells the container to run in detached mode.  

    sudo docker-compose up -d

Stop the container with the stop command:

    sudo docker stop ftps

Use other docker commands to start, remove, and otherwise administer the container.  On Linux systems, most commands will require that you run with `sudo`.

| Command | Description |
| ------- | ----------- |
| `docker-compose up -d` | Download and run the Docker container |
| `docker stop ftps` | Stop the ftps Docker container |
| `docker rm ftps` | Remove the Docker container from Docker | 
| `docker container logs ftps` | View the logs for this container |
| `docker exec -it ftps bash` | Execute a command in the container (in this case, a shell) |
| `docker ps -f name=ftps` | Check the status of this container |

## Using a Makefile to Issue Docker Commands

A Makefile is provided as a means for encapsulating a bunch of
useful Docker commands.  Docker commands can often become long
and somewhat complicated, so this makes them easier to execute.
The Makefile itself also serves as documentation of a few Docker
commands.

_Use of this Makefile to manage the container is optional._

Starting and stopping the container using the Makefile is easy:

    make start
	make stop

Available commands via the Makefile:

| Command | Description |
| ------- | ----------- |
| `make start` | Start the container |
| `make stop`  | Stop the container |
| `make rm` | Removes the container (after stopping it) |
| `make logs` | View the container's log file |
| `login` | Access the container's shell |
| `make status` | Display the container's status |

## Testing the Container

### Filezilla

One may use [Mozilla's Filezilla](https://filezilla-project.org/)
to connect to and transfer files to and from the FTPS container.

### lftp

A shell script that runs `lftp` is provided for convenience.
The `lftp` program is used to connect to an FTPS server and
transfer files back and forth.  This program can be installed
on an Ubuntu/Debian system with the following command:

    sudo apt install lftp

The script is as follows:

	#!/bin/bash
	USER=foofoo
	PASS=<SET YOUR PASSWORD HERE>

	HOST=localhost
	PORT=5000

	LOCAL_BACKUP_DIR='./files'
	REMOTE_DIR='/home/$USER'
	CMD='mirror -R -e "$LOCAL_BACKUP_DIR" "$REMOTE_DIR"'
	CMD='pwd'
	CMD='ls'

	lftp -u $USER,$PASS $HOST:$PORT <<EOF
	set ftp:ssl-protect-data true
	set ftp:ssl-force true
	set ssl:verify-certificate no
	$CMD
	quit
	EOF

Use the `put` lftp command to copy a file from the local machine 
to the FTPS server and the `get` command to retrieve a file from 
the server.

