#!/bin/bash
USER=foofoo
PASS=<SET YOUR PASSWORD HERE>

HOST=localhost
PORT=5000

LOCAL_BACKUP_DIR='./files'
REMOTE_DIR='/home/$USER'
CMD='mirror -R -e "$LOCAL_BACKUP_DIR" "$REMOTE_DIR"'
CMD='pwd'
CMD='ls'

lftp -u $USER,$PASS $HOST:$PORT <<EOF
set ftp:ssl-protect-data true
set ftp:ssl-force true
set ssl:verify-certificate no
$CMD
quit
EOF
